/*
  A Micro Lite Http File Server
  http://golang.org/pkg/net/http/#example_FileServer
  go build nano_httpd.go

  Added debug
  26-June-19 :
*/
package main

import (
	"flag"
	"log"
	"net/http"

	"fmt"

	"golang.org/x/crypto/acme/autocert"
)

func main() {
	inPort := flag.String("port", "7070", "Input Port Number, default 7070")
	flDir := flag.String("root", "~", "root content directory default is ~")
	domain := flag.String("domain", "", "domain mapped to public IP, like: qzip.in")

	flag.Parse()

	httpPort := ":" + *inPort
	fmt.Printf("Port %s, Doc Root [%s]\n", httpPort, *flDir)

	// Simple static webserver:
	if *domain == "" {
		log.Fatal(http.ListenAndServe(httpPort, http.FileServer(http.Dir(*flDir))))
	} else {
		if *flDir == "~" || *flDir == "/" {
			log.Fatal("for secured domain; root can't be ~ or /\n")
		} else {
			log.Fatal(http.Serve(autocert.NewListener(*domain), http.FileServer(http.Dir(*flDir))))
		}

	}

}
