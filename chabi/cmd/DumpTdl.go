package main

import (
	"chabi/parser"
	"flag"
	"fmt"
	"strings"
)

/**
  Inputs: In TDL File, Out STDIO , debug (true/false, default: false)
*/

func main() {
	undef := "undefined"
	fmt.Printf("TDL to JSON parser\n")
	var tdlFile = flag.String("tdl", undef, "Input TDL File")

	var debug = flag.Bool("debug", true, "Set debug=true to emit file and line numbers")
	flag.Parse()
	//fmt.Printf("%s , %s, %b \n", *tdlFile, *jsonFile, *debug)
	if strings.EqualFold(undef, *tdlFile) {
		fmt.Printf("Please specify both tdl  file. Example \nGenRawJson -tdl in.tdl -debug=true\n")
		return
	}
	err := tdlDump(*tdlFile, *debug)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("Success generated\n")
}

type dumper struct {
	debug  bool
	cntMax int
}

func tdlDump(tdlFile string, dbg bool) error {
	dump := new(dumper)
	return parser.ParseRaw(tdlFile, dump)
}

var Max = 100000

func (p *dumper) OnObject(header string, fileName string, lineNo int) error {
	p.cntMax += 1
	fmt.Printf("%d {\"_file_\":\"%s\", \"_line_\":%d , \"Object\":\"%s\", \"Attributes\": {[\n", p.cntMax, fileName, lineNo, header)
	if p.cntMax > Max {
		return fmt.Errorf("Max Count Exceeded")
	}
	return nil
}
func (p *dumper) OnAttribute(attrib string, fileName string, lineNo int) error {
	p.cntMax += 1
	fmt.Printf("%d {\"_file_\":\"%s\", \"_line_\":%d , \"Attribute\":\"%s\"}\n", p.cntMax, fileName, lineNo, attrib)
	if p.cntMax > Max {
		return fmt.Errorf("Max Count Exceeded")
	}
	return nil
}
func (p *dumper) End() {
	fmt.Println("]}")
}
