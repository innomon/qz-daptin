package main
 
import (
 "chabi/tdl/parser"
   "fmt"
   "flag"
   "strings"
)
/**
  Inputs: In TDL File, Out JSON File, debug (true/false, default: false)
*/

func main() {
   undef := "undefined"
   fmt.Printf("TDL to JSON parser\n")
   var tdlFile = flag.String("tdl",undef,"Input TDL File")
   var jsonFile = flag.String("json",undef,"Output JSON File")
  var debug = flag.Bool("debug", false, "Set debug=true to emit file and line numbers")
   flag.Parse()
   //fmt.Printf("%s , %s, %t \n", *tdlFile, *jsonFile, *debug)
   if strings.EqualFold(undef, *jsonFile) || strings.EqualFold(undef, *tdlFile) {
      fmt.Printf("Please specify both tdl and json files. Example \nGenRawJson -tdl in.tdl -json out.json -debug=true\n") 
      return 
   }
   err := parser.GenerateJson(*tdlFile, *jsonFile, *debug)
   if err != nil {
        fmt.Println(err)
        return 
   }
   fmt.Printf("Success generated %s",  *jsonFile)
}

