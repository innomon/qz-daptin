// AB 03-Dec-12
/*
   Entry Point function Parse("file name")
   We have a bottom up parsing strategy for TDL.
   The TDL Object Handlers should register their handlers with function RegisterObjectHandler(...) 
   
    "INCLUDE" Handling is hardwired. From language design prespective, INCLUDE is not an Object.  
*/
package parser

import (
    "strings"
    "errors"
    "fmt"
)
var ErrRightBracketExpected = errors.New("Right Bracket Expected")

type ObjectParser interface {
   OnObject(header string, fileName string, lineNo int) error
   OnAttribute(attrib string,  fileName string, lineNo int) error
   End()
}

var tdlParse *parser = new(parser) 

func RegisterObjectHandler(objName string, objParser ObjectParser ) {
     tdlParse.pMap[strings.ToUpper(objName)] = objParser
}

func Parse(tdlFile string) error {
   return ParseRaw(tdlFile, tdlParse)
}

type parser struct {
    curObjParser ObjectParser
    pMap map[string] ObjectParser
    
} 

func (p * parser) OnObject(header string, fileName string, lineNo int) error {
   if p.curObjParser != nil {
        p.curObjParser.End()
   }
   // check for Object Modifiers $, #, !
   oName := extractObjName(header)
   p.curObjParser = p.pMap[oName]
   if p.curObjParser == nil {
        return fmt.Errorf("No Registered Object Handler for [%s] ,file [%s] line %d",oName, fileName, lineNo)
   } 
   return p.curObjParser.OnObject(header, fileName, lineNo)
}  
// Page 31 TDL Ref Manual 
// $,  #,  ! 
func extractObjName(header string) string {
  hdr := strings.ToUpper(header)
  if StartsWith(hdr, "$#!") {
     hdr = string(hdr[1:len(hdr)])
  }
  return strings.Split(hdr,":")[0]
} 


func (p * parser) OnAttribute(attrib string,  fileName string, lineNo int) error{
   
   if p.curObjParser == nil {
        return fmt.Errorf("No Object defined for attribute,[%s] line %d",fileName, lineNo)
   } 
   return p.curObjParser.OnAttribute(attrib, fileName, lineNo) 
}   
func (p * parser)    End(){
   if p.curObjParser != nil {
        p.curObjParser.End()
   }
   p.curObjParser = nil
}   
   
func ParseRaw(tdlFile string, objParser ObjectParser) error {
    progFile, err := NewProgFile(tdlFile)
    defer objParser.End() 
    if err != nil {
        return err
    }
    inSlashDotComment := false // comments cannot span across files
    continuedLine := ""
    for  progFile.MoveNext() {
        lineInfo := progFile.GetCurLineInfo()
        if lineInfo == nil {
           break
        }    
        lineVal := lineInfo.LineValue()
        if len(lineVal) == 0 {
           continue
        }
        //  eat comments. TODO: ignore comments inside quote, that is, inside a string
        if strings.Index(lineVal, ";") == 0 {
            continue
        }  
        if strings.Index(lineVal, "/*") == 0 {
            if inSlashDotComment {
               return fmt.Errorf("Nested comments not allowed,[%s] line %d",lineInfo.FileName(), lineInfo.LineNumber())
            } 
            inSlashDotComment = true
            continue
        }   
        if inSlashDotComment {
           if strings.Index(lineVal, "*/") >= 0 {
              inSlashDotComment = false
           }
           continue
        }
        // Handle '+' (line continuation). if line ends with '+' it means line continuation.
        tmp := lineVal
        a := make([]string, 2)
        a[0] = continuedLine ; a[1] = tmp
        continuedLine = strings.Join(a,"")
 
        if len(tmp) > 0 && tmp[len(tmp) -1] == '+' {
          // remove the last '+'
          continuedLine = string(tmp[0:len(tmp) -1])
          continue;
        }       
        tmp = continuedLine
        continuedLine = ""
        
        // check if Object
        i := strings.Index(tmp, "[")
        if i == 0 {
            j := strings.Index(tmp,"]")
            if j == -1 {
                return ErrRightBracketExpected
            }  
            if j == 1 {
               return fmt.Errorf("Empty Object ,[%s] line %d",lineInfo.FileName(), lineInfo.LineNumber())
            }
            // strip []
            tmp = string(tmp[1:j])
            // Handle Include Here
            if strings.HasPrefix(strings.ToUpper(tmp), "INCLUDE") {
               coln := strings.Index(tmp, ":")
               if coln == -1 {
                  return fmt.Errorf("Colon : expected after Include ,[%s] line %d",lineInfo.FileName(), lineInfo.LineNumber())
               }
               if coln == (len(tmp) -1) {
                  return fmt.Errorf("File Name expected ,[%s] line %d",lineInfo.FileName(), lineInfo.LineNumber())
               } 
               flName := string(tmp[coln+1:]) 
               
               flName = AppendDirIfNotExists(flName, ExtractDirName(lineInfo.FileName()))    
                         
               er := progFile.IncludeFile(flName)
               if er != nil {
                    return er
               }
               continue
            }  
            err := objParser.OnObject(tmp, lineInfo.FileName(), lineInfo.LineNumber())
            if err != nil {
              return err
            }
            continue
        }
        err := objParser.OnAttribute(tmp, lineInfo.FileName(), lineInfo.LineNumber())
        if err != nil {
              return err
        }
    }
    return nil
} 


