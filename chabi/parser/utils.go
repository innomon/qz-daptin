package parser

import (
  "strings"
  "os"
)

func StartsWith(str string, items string) bool {
   ret := false
   b := str[0]
   for  i:= 0; i < len(items); i++ {
     if b == items[i] {
        ret = true
        break;
     }
   } 
   return ret
} 

func ExtractDirName(flname string ) string {
  fnam := strings.TrimSpace(flname)
  // replace Dos separator with Unix separator
  fnam  = strings.Replace(flname, "\\", "/", -1)
  
  dir := "./"
  dEnd := strings.LastIndex(fnam, "/")
  if  dEnd == -1 {
    return dir
  } 
  if fnam[0] != '/' {
    s := []string{ dir, fnam}
    fnam = strings.Join(s,"")
  }
  if(dEnd == 0) {
     return "/"
  }
  dir = string(fnam[0:dEnd])
  return dir
}

func AppendDirIfNotExists(fname string, dir string) string {
     fname = strings.TrimSpace(fname)
     fname  = strings.Replace(fname, "\\", "/", -1)
   _, err := os.Stat(fname)
   if err != nil {
      joiner := "/"
      if  dir[len(dir)-1] == '/' {  
         if len(dir) > 1 {
         joiner = ""
         }
      }
      if fname[0] == '/'{
         if len(fname) > 1 {
            fname = string(fname[1:])
         }
      }
      s := [] string { dir, fname}
      return strings.Join(s,joiner)
   }
   return fname
}


