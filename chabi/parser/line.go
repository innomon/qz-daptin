// AB 29-Nov-12
/*
 line parser and is unaware of any other higher level types/structures
 As a test, it should compile on its own, without any dependencies. 
*/
package parser
import (
 "bufio"
 "os"
 "io"
 "container/list"
 "strings"
// "fmt"
)
type LineInfo interface {
    FileName() string
    LineNumber() int
    LineValue() string
}
type ProgFile interface {
   IncludeFile(fileName string) error
   Front()
   MoveNext() bool
   GetCurLine() *string
   GetCurLineInfo() LineInfo
   
} 

func NewProgFile(fileName string) (ProgFile, error) {
     tf := new(tdlProg)
     tf.lineList = list.New()
     err := tf.IncludeFile(fileName)   
     tf.Front()
     return tf,err
}

type tdlProg struct {
    lineList *list.List
    curEle * list.Element
}
// TODO Optimize the design to eleminate filename overhead for every line
type line struct {
    fname string
    lineno int
    val string
}

func (tdlP * tdlProg) IncludeFile(fileName string) error {
 //  fmt.Printf("Include [%s]\n",fileName)
   file, err := os.Open(fileName)
   if err != nil {
       return err
   }
   defer file.Close()
 
   rd := bufio.NewReader(file)
   curEle := tdlP.curEle 
   for i:=1;;i++ {
       ln, err := rd.ReadString('\n')
       if err != nil && err != io.EOF {
          return err
       }
       lp := new(line)
       lp.lineno = i
       lp.val = strings.TrimSpace(ln)
       lp.fname = fileName
       // Add the Line to list
       if curEle == nil {
          curEle = tdlP.lineList.PushFront(lp)
          tdlP.curEle = curEle
          continue   
       }
       curEle = tdlP.lineList.InsertAfter(lp, curEle)   
       if err == io.EOF {
          break;
       }    
   }
    
    return nil
}
func (tdlP * tdlProg) Front() {
   tdlP.curEle = tdlP.lineList.Front()
}   
func (tdlP * tdlProg) MoveNext() bool {
    tdlP.curEle =  tdlP.curEle.Next()
    return tdlP.curEle != nil
}
func (tdlP * tdlProg) GetCurLine() *string {
    if tdlP.curEle == nil {
       return nil
    }
    curL :=  tdlP.curEle.Value.(line).val
    return &curL
}
func (tdlP * tdlProg) GetCurLineInfo() LineInfo {
    return tdlP.curEle.Value.(LineInfo)
}

func (l *line) FileName() string {
   return l.fname
}
func (l *line) LineNumber() int {
   return l.lineno
}
func (l *line) LineValue() string {
   return l.val
}

