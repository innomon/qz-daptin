// AB 27-Dec-12
package parser

import (
  "os"
  "io"
  "fmt"
  "strings"
)
  
func GenerateJson(tdlFile string, jsonFile string, debug bool) error {
   jsonF, err := os.Create(jsonFile)
   if err != nil {
       return err
   }
   defer jsonF.Close()
    r2j := newRaw2Json(jsonF, debug)
    
    fmt.Fprintf(jsonF, "{ \"TDL\": {\n")
    err = ParseRaw(tdlFile, r2j)
    fmt.Fprintf(jsonF, "}}\n")
    return err
}

type raw2json struct {
    debug bool
    attributesSeen bool
    out io.Writer
}

func newRaw2Json(writer io.Writer, dbg bool) *raw2json {
   r2j := new(raw2json)
   r2j.debug = dbg
   r2j.out = writer
   return r2j
}
/* At this point the square prackets have been stripped. so we have
    <object> : <name>
    Object may have modifiers
    JSON syntax:
    { "Object": { "ObjectType": <type>, "Name": "<name>", "Modifier": <modifier>, "Attributes":[ <attribute>, ...] }} 
    <attribute> --> 
       "Attribute": [ <values>,..]
*/ 
func (p * raw2json) OnObject(header string, fileName string, lineNo int) error {
    if p.attributesSeen {
       p.End()
    }
    p.attributesSeen = false
    p.printDebug(fileName, lineNo)
    fmt.Fprintf(p.out, "{ \"Object\": { \n")
    colon := strings.Index(header,":")
    objTitle := "Undefined"
    if colon > 1 && colon < len(header) {
       objTitle = string(header[colon+1:])
    } 
    fmt.Fprintf(p.out, "\t\"ObjectType\": \"%s\", \"Name\":\"%s\",",extractObjName(header),strings.TrimSpace(objTitle))
    // Modifiers see TABLE 3.2 General Symbols, page 32, TDL Ref Manual
    switch header[0] {
       case '#':
         fmt.Fprintf(p.out,"\"Modifier\": \"Definition\",") 
       case '!':
         fmt.Fprintf(p.out,"\"Modifier\": \"Optional\",") 
       case '*':
         fmt.Fprintf(p.out,"\"Modifier\": \"Reinitialize\",")         
    }  
    fmt.Fprintf(p.out," \"Attributes\":[\n")
      
      
return nil
}

func (p * raw2json) OnAttribute(attrib string,  fileName string, lineNo int) error {    
    p.printDebug(fileName, lineNo)
    // TODO: preserve colon ':' within string
    attribs := strings.Split(attrib, ":")
    if p.attributesSeen {
        fmt.Fprintf(p.out,",")
    }
    fmt.Fprintf(p.out,"\"Attribute\" :[ ")
    first := true
    for i := range attribs {
       if !first {
          fmt.Fprintf(p.out,", ")
       }     
       atr := strings.Replace(strings.TrimSpace(attribs[i]),"\"","\\\"",-1)
       fmt.Fprintf(p.out,"\"%s\"", atr)

       first = false
    }
    
    fmt.Fprintf(p.out," ]\n")
    p.attributesSeen = true
return nil
}
func (p * raw2json) End() {
    fmt.Fprintf(p.out, "]}}\n")  // close object
}

func (p * raw2json) printDebug(fileName string, lineNo int) {
    if p.debug {
       fmt.Fprintf(p.out, "{ \"_tdl_file_\": \"%s\", \"_line_\": %d}\n",fileName, lineNo)  
    }
}   
   
