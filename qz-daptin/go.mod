module qz-daptin

go 1.12

replace (
	github.com/gin-gonic/gin/json => ../gin-json/
	gopkg.in/Masterminds/squirrel.v1 v1.1.0 => github.com/Masterminds/squirrel v1.1.0

)

require (
	github.com/GeertJohan/go.rice v1.0.0
	github.com/PuerkitoBio/goquery v1.5.0 // indirect
	github.com/advance512/yaml v0.0.0-20141213031416-e401b2b02685 // indirect
	github.com/alexeyco/simpletable v0.0.0-20190222165044-2eb48bcee7cf // indirect
	github.com/araddon/dateparse v0.0.0-20190510211750-d2ba70357e92 // indirect
	github.com/artpar/api2go v2.4.0+incompatible // indirect
	github.com/artpar/api2go-adapter v1.0.0 // indirect
	github.com/artpar/conform v0.0.0-20180710100920-3c5c27338a81 // indirect
	github.com/artpar/go.uuid v1.2.0 // indirect
	github.com/artpar/rclone v0.0.0-20190429081623-dc525e165a5e // indirect
	github.com/artpar/resty v1.0.1 // indirect
	github.com/artpar/stats v0.0.0-20190329104810-3a7266112683 // indirect
	github.com/asaskevich/EventBus v0.0.0-20180315140547-d46933a94f05 // indirect
	github.com/corpix/uarand v0.1.0 // indirect
	github.com/daptin/daptin v0.7.1-0.20190429082847-417a2c1baae7
	github.com/dlclark/regexp2 v1.1.6 // indirect
	github.com/dop251/goja v0.0.0-20190603191204-1b2d25ba9a8d // indirect
	github.com/etgryphon/stringUp v0.0.0-20121020160746-31534ccd8cac // indirect
	github.com/flashmob/go-guerrilla v0.0.0-20190504150445-87453fe54c89 // indirect
	github.com/gin-contrib/static v0.0.0-20190511124741-c1cdf9c9ec7b // indirect
	github.com/gin-gonic/gin v1.4.0
	github.com/gin-gonic/gin/json v0.0.0-00010101000000-000000000000 // indirect
	github.com/go-playground/locales v0.12.1 // indirect
	github.com/go-playground/universal-translator v0.16.0 // indirect
	github.com/go-sourcemap/sourcemap v2.1.2+incompatible // indirect
	github.com/go-sql-driver/mysql v1.4.1 // indirect
	github.com/gocarina/gocsv v0.0.0-20190426105157-2fc85fcf0c07 // indirect
	github.com/gocraft/health v0.0.0-20170925182251-8675af27fef0
	github.com/gonum/blas v0.0.0-20181208220705-f22b278b28ac // indirect
	github.com/gonum/floats v0.0.0-20181209220543-c233463c7e82 // indirect
	github.com/gonum/internal v0.0.0-20181124074243-f884aa714029 // indirect
	github.com/gonum/lapack v0.0.0-20181123203213-e4cdc5a0bff9 // indirect
	github.com/gonum/matrix v0.0.0-20181209220409-c518dec07be9 // indirect
	github.com/gonum/stat v0.0.0-20181125101827-41a0da705a5b // indirect
	github.com/graphql-go/graphql v0.7.8 // indirect
	github.com/graphql-go/handler v0.2.3 // indirect
	github.com/graphql-go/relay v0.0.0-20171208134043-54350098cfe5 // indirect
	github.com/icrowley/fake v0.0.0-20180203215853-4178557ae428 // indirect
	github.com/jamiealquiza/envy v1.1.0
	github.com/jinzhu/copier v0.0.0-20180308034124-7e38e58719c3 // indirect
	github.com/jmoiron/sqlx v1.2.0 // indirect
	github.com/kniren/gota v0.9.0 // indirect
	github.com/leodido/go-urn v1.1.0 // indirect
	github.com/lib/pq v1.1.1 // indirect
	github.com/looplab/fsm v0.1.0 // indirect
	github.com/mattn/go-sqlite3 v1.10.0 // indirect
	github.com/pquerna/otp v1.2.0 // indirect
	github.com/robfig/cron v1.1.0 // indirect
	github.com/sadlil/go-trigger v0.0.0-20170328161825-cfc3d83007cd
	github.com/sirupsen/logrus v1.4.2 // indirect
	github.com/spf13/cobra v0.0.5 // indirect
	github.com/spf13/viper v1.4.0 // indirect
	github.com/tealeg/xlsx v1.0.3 // indirect
	github.com/thoas/stats v0.0.0-20190407194641-965cb2de1678 // indirect
	golang.org/x/crypto v0.0.0-20190411191339-88737f569e3a
	golang.org/x/oauth2 v0.0.0-20190604053449-0f29369cfe45 // indirect
	gopkg.in/Masterminds/squirrel.v1 v1.1.0 // indirect
	gopkg.in/go-playground/validator.v9 v9.29.0 // indirect
	gopkg.in/src-d/go-git.v4 v4.11.0 // indirect
)
