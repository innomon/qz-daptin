package main 

	
import (
 "os"
 "fmt"
 "strings"
 "io/ioutil"
 "path/filepath"
 xj "github.com/innomon/goxml2json"
)

var root = ""
var outDir = ""

func main() {
	if len(os.Args) != 3 {
		fmt.Println("usage: ", os.Args[0], " <in file or dir name> <dir>")
		return
	}
	outDir = os.Args[2]
	root = os.Args[1]
    if !strings.HasSuffix(outDir, "/") {
		outDir = outDir+ "/"
	}
    fi, err := os.Lstat(root)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	
 
    if fi.Mode().IsDir() {
		if err = traverseDir(root); err != nil {
			fmt.Println(err.Error())
			return			
		}
	} else {
		flname := root 	
		x2j(flname )
	}

}



func mkdirs(path string ) error {
	if _, err := os.Stat(path); !os.IsNotExist(err) {
		return err
	}
	err := os.MkdirAll(path, os.ModePerm) 
	return	err	

}
func  traverseDir(inDir string)( err error)  {

	err = filepath.Walk(inDir, func(path string, info os.FileInfo, ferr error) error {
		if ferr != nil {
			return ferr
		}
		fmt.Println("inDir:", inDir, "path: ", path)
		if inDir == path {
			return nil
		}
		if strings.HasSuffix(path, ".") { return nil} // . .. cases
		/*ndx := strings.Index(path, inDir)
		if ndx <= 0 {
			return fmt.Errorf("inDir: [%v] path: [%v] mismatch ", inDir, path)
		}*/
		//newOutDir := outDir + "/" + path // [ndx:] 
		if info.IsDir() {
			return traverseDir(path) 
		}
		
		return x2j(path)

	}) // end walk
	return 
}
func x2j( infl string) error {

	if !strings.HasSuffix(strings.ToLower(infl), ".xml") {
		return nil
	}
    if len(infl) < 6 {
		return fmt.Errorf("too short file name [%v]", infl)
	} 
	fmt.Println("x2j => root" ,root,  "infl:", infl, "outDir:", outDir)
	outFileName := outDir // already has trailing / inserted

	if strings.HasPrefix(infl, "/") {
		outFileName = outFileName + infl[1:strings.LastIndex(infl, ".")] + ".json"
	} else {
		outFileName = outFileName + infl[:strings.LastIndex(infl, ".")] + ".json"
	}
	
	ndx :=  strings.LastIndex(outFileName, "/")
	if ndx > 0 {
		outFileDir := outFileName[:ndx]
		if err := mkdirs(outFileDir); err != nil  {
			return err
		}
	} 
		
	xml, err := ioutil.ReadFile(infl)
	if err != nil {
		return err
	}
	xr := strings.NewReader(string(xml))
	json, err := xj.Convert(xr)
  	if err != nil {
  		return err
	}
   
	err = ioutil.WriteFile(outFileName, json.Bytes(), 0644)
	return err
	//return nil
}

/*
func Convert(r io.Reader) (*bytes.Buffer, error) {
	// Decode XML document
	root := &xj.Node{}
	dec := xj.NewDecoder(r)
	dec.SetAttributePrefix("attr")
	err := dec.Decode(root)
	if err != nil {
		return nil, err
	}

	// Then encode it in JSON
	buf := new(bytes.Buffer)

	enc := xj.NewEncoder(buf)
	enc.SetAttributePrefix("attr")
	err = enc.Encode(root)
	if err != nil {
		return nil, err
	}

	return buf, nil
}
*/