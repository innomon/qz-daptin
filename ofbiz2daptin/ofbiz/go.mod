module ofbiz

go 1.12

replace github.com/innomon/goxml2json v1.1.0 => ../goxml2json

require github.com/innomon/goxml2json v1.1.0
